var Ajv = require('ajv')
var ajvFormats = require('ajv-formats')
var fetch = require('node-fetch')

function loadSchema(uri) {
    return fetch(uri)
        .then(response => response.json())
}

const ajv = new Ajv({ loadSchema: loadSchema, allErrors: true, strict: 'log' })
ajvFormats(ajv)

var getValidatorForSchemaId = (schemaId) => {
    return Promise
        .resolve(ajv.getSchema(schemaId))
        .then(schema => {
            if(schema) {
                return schema
            } else {
                return fetch(schemaId)
                    .then(response => response.json())
                    .then(schema => ajv.compileAsync(schema))
            }
        })
}

module.exports.getValidatorForSchemaId = getValidatorForSchemaId
module.exports.ajv = ajv
module.exports.errors = ajv.errors
