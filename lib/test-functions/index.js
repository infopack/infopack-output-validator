var fs = require('fs')
var path = require('path')
var Promise = require('bluebird')
const _ = require('lodash')

var ajv = require('../ajv')
const { FetchError } = require('node-fetch')

function hasMainFile(test, packagePath) {
    return Promise
        .resolve()
        .then(() => {
            let mainPath = test.path.replace('.meta.json', '')
            return {
                test: 'hasMainFile',
                valid: fs.existsSync(path.join(packagePath, mainPath)),
                path: test.path,
                message: 'main file is missing',
                messages: ['main file is missing'],
                numberOfMessages: 1,
                level: test.level
            }
        })
}

module.exports.hasMainFile = hasMainFile

function hasSidecarFile(test, packagePath) {
    return Promise
        .resolve()
        .then(() => {
            let sidecarPath = path.join(packagePath, test.path + '.meta.json')
            return {
                test: 'hasSidecarFile',
                valid: fs.existsSync(sidecarPath),
                path: test.path,
                message: 'sidecar file is missing',
                messages: ['sidecar file is missing'],
                numberOfMessages: 1,
                level: test.level
            }
        })
}

module.exports.hasSidecarFile = hasSidecarFile

function validAgainstSchema(test, packagePath) {
    return Promise
        .resolve()
        .then(() => {
            let data
            try {
                data = JSON.parse(fs.readFileSync(path.join(packagePath, test.path)).toString())
            } catch (error) {
                throw error
            }  
            return data
        })
        .then(data => Promise.all([data, ajv.getValidatorForSchemaId(test.options.schema)]))
        .then(([data, validate]) => Promise.all([data, validate, validate(data)]))
        .then(([data, validate, isValid]) => {
            if(isValid) {
                return {
                    test: 'validAgainstSchema',
                    valid: true,
                    path: test.path,
                    level: test.level
                }
            } else {
                return {
                    test: 'validAgainstSchema',
                    valid: false,
                    path: test.path,
                    message: validate.errors[0].message,
                    messages: validate.errors,
                    numberOfMessages: validate.errors.length,
                    level: test.level
                }
            }
        })
        .catch(err => {
            if(err instanceof SyntaxError) {
                return {
                    test: 'validAgainstSchema',
                    valid: false,
                    path: test.path,
                    message: 'Not a valid JSON file',
                    messages: ['Not a valid JSON file'],
                    numberOfMessages: 1,
                    level: test.level
                }
            } else {
                throw err
            }
        })
}

module.exports.validAgainstSchema = validAgainstSchema

/**
 * Validates against the $schema property
 */
function validAgainstOwnSchema(test, packagePath) {
    let result = {
        level: test.level,
        test: test.name,
        path: test.path
    }
    return Promise
        .resolve()
        .then(() => {
            let data
            try {
                data = JSON.parse(fs.readFileSync(path.join(packagePath, test.path)).toString())
            } catch (error) {
                throw error
            }  
            return data
        })
        .then(data => {
            if(!data.$schema) {
                throw new Error('no_schema')
            }
            return data
        })
        .then(data => Promise.all([data, ajv.getValidatorForSchemaId(data.$schema)]))
        .then(([data, validate]) => Promise.all([data, validate, validate(data)]))
        .then(([data, validate, isValid]) => {
            if(isValid) {
                result.valid = true
                return result
            } else {
                result.valid = false
                result.message = validate.errors[0].message
                result.messages = validate.errors
                result.numberOfMessages = validate.errors.length
                return result
            }
        })
        .catch(err => {
            if(err.name == 'no_schema') {
                result.valid = false
                result.message = '$schema property is missing'
                result.messages = ['$schema property is missing']
                result.numberOfMessages = 1
                return result
            } else if(err instanceof FetchError) {
                result.valid = false
                result.message = 'Could not resolve schema from $schema property',
                result.messages = ['Could not resolve schema from $schema property']
                result.numberOfMessages = 1
                return result
            } else {
                throw err
            }
        })
}

module.exports.validAgainstOwnSchema = validAgainstOwnSchema

function validJsonFile(test, packagePath) {
    return Promise
        .resolve()
        .then(() => {
            let data
            try {
                data = JSON.parse(fs.readFileSync(path.join(packagePath, test.path)).toString())
            } catch (error) {
                throw error
            }  
            return {
                test: test.name,
                valid: true,
                path: test.path,
                message: 'OK',
                messages: ['OK'],
                numberOfMessages: 1,
                level: test.level
            }
        })
        .catch(err => {
            if(err instanceof SyntaxError) {
                return {
                    test: test.name,
                    valid: false,
                    path: test.path,
                    message: 'Not a valid JSON file',
                    messages: ['Not a valid JSON file'],
                    numberOfMessages: 1,
                    level: test.level
                }
            } else {
                throw err
            }
        })
}

module.exports.validJsonFile = validJsonFile

function checkCapitalization(test, packagePath) {
    if(test.path.toLowerCase() === test.path) {
        return {
            test: test.name,
            valid: true,
            path: test.path,
            message: 'OK',
            messages: ['OK'],
            numberOfMessages: 1,
            level: test.level
        }
    } else {
        return {
            test: test.name,
            valid: false,
            path: test.path,
            message: 'filename contains uppercase letters',
            messages: ['filename contains uppercase letters'],
            numberOfMessages: 1,
            level: test.level
        }
    }
}

module.exports.checkCapitalization = checkCapitalization

function get(name) {
    return module.exports[name]
}

module.exports.get = get
