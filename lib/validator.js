var Promise = require('bluebird')
var fg = require('fast-glob')
const _ = require('lodash')

var testSuites = require('./test-suites')
const testFunctions = require('./test-functions')

function Validator(packagePath, options) {

    this._manifestVersion = undefined
    this._packagePath = packagePath
    this._options = options || {}
    this._valids = []
    this._errors = []
    this._critical = []
    this._info = []
    this._valid = undefined
    this._tests = []
    this._testInstances = []

    this.addTestSuite = function(manifestVersion) {
        this._manifestVersion = manifestVersion
        // load tests
        this._tests = testSuites[this._manifestVersion]
        return this
    }
    this.addTest = function(test) {
        this._tests.push(test)
        return this
    }

    this.isValid        = () => this._critical.length < 1
    this.getErrors      = () => this._errors
    this.getCritical    = () => this._critical
    this.getInfo        = () => this._info
    this.getErrorsCount = () => this._errors.length
    this.getValids      = () => this._valids
    this.getValidsCount = () => this._valids.length

    /**
     * Perform the validation
     * @returns Promise
     */
    this.run = () => {
        return Promise
            .resolve()
            .then(() => {
                if(this._tests.length === 0) {
                    throw new Error('no_test_cases')
                }
                console.log(`Tests: ${this._tests.length}`)
            })
            .then(() => Promise.mapSeries(this._tests, test => {
                return fg(test.patterns, { cwd: this._packagePath })
                    .then(paths => {
                        console.log(`Files for test ${test.name}: ${paths.length}`)
                        return _.map(paths, path => {
                            return { path: path, name: test.name, options: test.options || {}, level: test.level }
                        })
                    })
            }))
            .then(groupedTestInstances => _.flatten(groupedTestInstances))
            .tap(testInstances => this._testInstances = testInstances)
            .tap(testInstances => console.log(`Test instances: ${testInstances.length}`))
            .then(testInstances => Promise.mapSeries(testInstances, testInstance => testFunctions.get(testInstance.name)(testInstance, this._packagePath)))
            .tap(results => {
                this._errors = _.filter(results, { valid: false })
                this._critical = _.filter(results, { valid: false, level: 'critical' })
                this._info = _.filter(results, { valid: false, level: 'info' })
                this._valids = _.filter(results, { valid: true })
            })
            .catch(err => {
                console.log(err)
            })
    }
}

module.exports = Validator
