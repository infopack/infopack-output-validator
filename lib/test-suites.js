module.exports = {
    "0.0.1": [{
        patterns: ['index.html'],
        name: 'exists',
        level: 'critical'
    }, {
        patterns: ['assets/output.json'],
        name: 'validAgainstSchema',
        options: { schema: '' },
        level: 'critical'
    }, {
        patterns: ['assets/infopack.json'],
        name: 'validAgainstSchema',
        options: { schema: '' },
        level: 'critical'
    }],
    "1.0.0": [{
        patterns: ['infopack.json'],
        name: 'validAgainstSchema',
        options: { schema: 'https://schemas.infopack.io/infopack-infopack.1.schema.json' },
        level: 'critical'
    }, {
        patterns: ['index.json'],
        name: 'validAgainstSchema',
        options: { schema: 'https://schemas.infopack.io/infopack-index.1.schema.json' },
        level: 'critical'
    }, {
        patterns: ['**/*.json', '!**/*.meta.json', '!index.json', '!infopack.json'],
        name: 'validJsonFile',
        level: 'critical'
    }, {
        patterns: ['**/*.meta.json'],
        name: 'hasMainFile',
        level: 'critical'
    }, {
        patterns: ['**/*.meta.json'],
        name: 'validAgainstSchema',
        options: { schema: 'https://schemas.infopack.io/infopack-meta.1.schema.json' },
        level: 'critical'
    }, {
        patterns: ['**/*', '!**/*.meta.json', '!**/*.meta.html', '!index.json', '!index.html', '!infopack.json'],
        name: 'hasSidecarFile',
        level: 'critical'
    }, {
        patterns: ['**/*'],
        name: 'checkCapitalization',
        level: 'info'
    }],
    "2.0.0": [{
        patterns: ['infopack.json'],
        name: 'validAgainstSchema',
        options: { schema: 'https://schemas.infopack.io/infopack-infopack.1.schema.json' },
        level: 'critical'
    }, {
        patterns: ['index.json'],
        name: 'validAgainstSchema',
        options: { schema: 'https://schemas.infopack.io/infopack-index.2.schema.json' },
        level: 'critical'
    }, {
        patterns: ['**/*.json', '!**/*.meta.json', '!index.json', '!infopack.json'],
        name: 'validJsonFile',
        level: 'critical'
    }, {
        patterns: ['**/*.meta.json'],
        name: 'hasMainFile',
        level: 'critical'
    }, {
        patterns: ['**/*.meta.json'],
        name: 'validAgainstSchema',
        options: { schema: 'https://schemas.infopack.io/infopack-meta.2.schema.json' },
        level: 'critical'
    }, {
        patterns: ['**/*', '!**/*.meta.json', '!**/*.meta.html', '!index.json', '!index.html', '!infopack.json'],
        name: 'hasSidecarFile',
        level: 'critical'
    }, {
        patterns: ['**/*'],
        name: 'checkCapitalization',
        level: 'info'
    }]
}
