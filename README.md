# Infopack Output Validator

This library can be used to validate that an infopack output follows all the rules stated in the [infopack framework](https://gitlab.com/infopack/infopack-framework).
