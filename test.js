var path = require('path')
const _ = require('lodash')
const { printTable } = require('console-table-printer')

var Validator = require('./lib/validator')

validator = new Validator(path.resolve('test-data', 'file-missing'));

validator
    .addTestSuite("2.0.0")
    .addTest({
        patterns: ['**/*.concept.json'],
        name: 'validAgainstOwnSchema'
    })
    .run()
    .then(data => {
        console.log(`Total number of test results: ${data.length}`)
        console.log(`Number of Valid: ${validator.getValidsCount() }`)
        console.log(`Number of Errors: ${validator.getErrorsCount() }`)
        console.log('== ERRORS ==')
        console.log(validator.getErrors())
        printTable(_.map(validator.getErrors(), e => { return { test: e.test, path: e.path, firstMessage: e.message, numberOfMessage: e.numberOfMessages, errorLevel: e.level } }))
    })
    .then(() => {
        console.log('Is valid?: ', validator.isValid())
    })
