# Kvalificerare för egenskaper
Denna värdelista baseras på metoden Kvalificerare för egenskaper som ni finner

Nedan följer Tabell 8 från SEK Handbok, med förtydligande kommentarer anpassade för skedena i byggprocessen. Koden kommer från värdelistor i IEC:s öppna databas.

aoaToTable[](./data/kvalificerare-for-egenskaper.json)
