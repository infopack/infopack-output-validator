# Bestämningsgrad

Följande värdelista är en del av metoden Informationsbeteckning- och beskrivning som ger svenska anvisningar för tillämpning av **prEN 17412-1 Building Information Modelling – Level of Information Need – Concepts and principles**. Det visar hur man i en leverans­specifikation ger informationsleveransen en beteckning som övergripande beskriver inne­hållet, och sedan i detalj beskriver innehållet.

Bestämningsgrad är en sammanfattande beteckning på hur långt huvuddelen av innehållet i informations­mängden är bestämt avseende geometrisk och alfanumerisk information. Enskilda objekt –system och komponenter – kan ha avvikande bestämningsgrad.

aoaToTable[](./data/bestamningsgrad.json)