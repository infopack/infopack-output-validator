# Skeden

Följande värdelista är en del av metoden Informationsbeteckning- och beskrivning som ger svenska anvisningar för tillämpning av **prEN 17412-1 Building Information Modelling – Level of Information Need – Concepts and principles**. Det visar hur man i en leverans­specifikation ger informationsleveransen en beteckning som övergripande beskriver inne­hållet, och sedan i detalj beskriver innehållet.

Beteckningen för skede visar vilket steg i livscykeln som informationen är avsedd att användas inom. Som exempel får man i produktionen enbart använda informationsmängder som har 4 som första siffra. Följande skeden gäller enligt denna metodbeskrivning:

aoaToTable[](./data/skeden.json)
