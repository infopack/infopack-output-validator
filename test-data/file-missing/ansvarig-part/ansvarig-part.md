# Ansvarig part

Tabeller nedan redovisas beteckningar för ansvariga parter i livscykelinformationsprocessen. Beteckningar används främst i projektering och produktion.

Kompletteras vid behov av informationssamordnare.

Beteckning baseras helt eller delvis på SS32202:2011 och branschpraxis.

## Projektörer

Baseras på SS32202:2011 (Kap. 2.1.1)

aoaToTable[Lista på samtliga beteckningar för projektörer](./data/projektorer.json)

## Entreprenörer

Baseras på SS32202:2011 (Kap. 2.1.2)

aoaToTable[Lista på samtliga beteckningar för entreprenörer](./data/entreprenorer.json)

## Beställare och dess medverkande

Baseras på SS32202:2011 (Kap. 2.1.3)

aoaToTable[Lista på samtliga beteckningar för beställare och dess medverkande](./data/bestallare-och-dess-medverkande.json)

## Kompletteringar utöver standard

Baseras på Nationella Riktlinjers rekommendationer och branschpraxis

aoaToTable[Lista på samtliga beteckningar för nationella riktlinjers kompletteringar](./data/kompletteringar-utover-standard.json)
