# Avfallskoder

Denna värdelista innehåller en förteckning över olika typer av avfall. För varje avfallstyp anges en sexsiffrig avfallskod. Avfallskoder som är markerade med en asterisk (*) anger att avfallstypen är farligt avfall.

Avfallstyperna är indelade i kapitel (tvåsiffrig kod) och underkapitel (fyrsiffrig kod).

Olika slags avfall som uppkommer vid en och samma anläggning kan behöva identifieras i olika avfallstyper.

aoaToTable[Tabellen redovisar koder för avfallstyper](./data/avfallskoder.json)

# Uttryck i bilagan

I denna bilaga avses med:

**Farligt ämne**
Ett ämne som är klassificerat som farligt till följd av att det uppfyller kriterierna i delarna 2-5 i bilaga I till förordning (EG) nr 1272/2008

**Tungmetall**
Föreningar av antimon, arsenik, kadmium, krom (VI), koppar, bly, kvicksilver, nickel, selen, tellur, tallium och tenn, samt dessa ämnen i metallisk form om de klassificerats som farliga ämnen

**Övergångsmetaller**
Föreningar av skandium, vanadin, mangan, kobolt, koppar, yttrium, niob, hafnium, volfram, titan, krom, järn, nickel, zink, zirkonium, molybden och tantal, samt dessa ämnen i metallisk form om de är klassificerade som farliga ämnen

**Stabilisering**
Process som ändrar avfallsbeståndsdelarnas farlighet, varvid farligt avfall omvandlas till icke-farligt avfall

**Solidifiering**
Process där endast avfallets aggregationstillstånd ändras genom tillsatser utan att avfallets kemiska egenskaper påverkas

**Delvis stabiliserat avfall**
Avfall som efter stabiliseringsprocessen fortfarande innehåller farliga beståndsdelar som inte fullständigt omvandlats till icke-farliga beståndsdelar och som kan avges till miljön på kort, medellång eller lång sikt
