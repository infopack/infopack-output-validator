# Redovisningssätt

Tabellerna nedan redovisar beteckningar för redovisningssätt vid ritningsnumrering och namngivning av filer, samt innehåll i modeller, enligt Bygghandlingar 90 Del 8, och ritningar enligt SS 32271:2016 med kompletteringar.

## Modeller (2D-, 3D-, objektmodeller) och basfiler

Baseras på Bygghandlingar 90 Del 8

aoaToTable[](./data/modeller-och-basfiler.json)

## Ritningar och ritningsdefinitioner

Baseras på SS 32271:2016

aoaToTable[](./data/ritningar-och-ritningsfiler.json)

## Eventuella kompletteringar

Baseras på Bygghandlingar 90 Del 8

aoaToTable[](./data/eventuella-kompletteringar.json)
