# Utseende

Följande värdelista är en del av metoden Informationsbeteckning- och beskrivning som ger svenska anvisningar för tillämpning av **prEN 17412-1 Building Information Modelling – Level of Information Need – Concepts and principles**. Det visar hur man i en leverans­specifikation ger informationsleveransen en beteckning som övergripande beskriver inne­hållet, och sedan i detalj beskriver innehållet.

Utseende: visuell representation, från symbolisk till realistisk. Här föreslås följande nivåer:



Förkortningen (0 osv.) används för maskinläsning, medan klartext (Ingen osv.) används för mänsklig läsning.