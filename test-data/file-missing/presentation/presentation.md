# Presentation

Presentationskoden klassificerar lager med ledning av, och typ av grafik CAD-lagret innehåller. Presentationskoden anges i Position 9 och 10 (när ytterligare indelning erfordras).

För att underlätta användningen bör lagerindelningen hålla en enhetlig nivå på alla CAD-filer i ett projekt eller i en modell. Nivåindelningen bör begränsas till vad som är nödvändigt för att åstadkomma olika typer av presentation eller ritningar.

aoaToTable[*Avser Tabell 5.11a i boken ”SB-Rekommendationer 11 – CAD-lager”, sid 90-91.*](./data/presentation-niva-indelning.json)

aoaToTable[*Avser Tabell 5.11b i boken ”SB-Rekommendationer 11 – CAD-lager”, sid 91-94.*](./data/presentation-presentationskod.json)
