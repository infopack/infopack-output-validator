# Dokumentklassificering

Dokumentklassificering utgår från [Bygg- och fastighetssektorns rekommendationer för dokumenthantering med metadata](https://www.bimalliance.se/for-dig-inom-bygg-och-forvaltning/standarder-for-digital-informationshantering/metadata-for-dokumenthantering-metadata-se/) och anger dokumentets användning och ska användas för dokument och inte för ritningar.

## Administrativa dokument
Dokument som används i stödjande aktiviteter till kärnprocessen i en organisation.


## Tekniska dokument
Dokument som beskriver byggnadsverk och processer för deras utformning, uppförande och användning.


## Juridiska eller ekonomiska dokument
Dokument som berör projektets eller byggnadsverkets ekonomiska eller juridiska relationer.


## Referensdokument
Externa dokument som används som kunskapskälla eller underlag i en process.
