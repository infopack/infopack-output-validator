# Säkerhetsklass

Information klassificeras utifrån konsekvenser som oönskad påverkan på informationens kvalitet bedöms leda till. Bedömningen ger fem klasser:

0 – Ingen eller försumbar skada

1 – Måttlig skada

2 – Betydande skada

3 – Allvarlig skada

4 – Synnerligen allvarlig skada – Information som omfattas av sekretess och rör Sveriges säkerhet (hemliga uppgifter). Vid hantering av hemliga uppgifter ska Säkerhetsskyddslagstiftningen och Säkerhetsskyddsförordningen beaktas.
