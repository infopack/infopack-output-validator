# Våningsplan

Våningsplan anges på olika sätt beroende på var de anges. Exempelvis kan dess ID anges i ritningsnumrering, Beteckning i modeller, och Benämning på ritningar. Nedan tabell redovisar systematik för detta. Tabellens omfattning ökar eller minskar beroende på projektets storlek.

aoaToTable[](./data/vaningsplan.json)

Projektets arkitekt ansvarar för våningsplan och ska följa ovan systematik.