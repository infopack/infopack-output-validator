# Syften

Följande värdelista är en del av metoden Informationsbeteckning- och beskrivning som ger svenska anvisningar för tillämpning av **SS-EN 17412-1 Building Information Modelling – Level of Information Need – Concepts and principles**. Det visar hur man i en leverans­specifikation ger informationsleveransen en beteckning som övergripande beskriver inne­hållet, och sedan i detalj beskriver innehållet.

En informationsmängd kan ha flera potentiella syften. Syftet med beteckningen är att visa för vilket syfte innehållet är godkänt att använda. I de fall flera syften är godkända läggs flera siffror till, separerade med semikolon. Nedan listas huvudgrupper med typiska handlingar. Hela 10-tal är fasta, medan 1-tal är förslag på finare indelning. Sådan skapas efter behov i varje projekt.

aoaToTable[](./data/syften.json)
