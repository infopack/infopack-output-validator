# Färgkodning IFC-modeller

Följande färgkoder rekommenderas för IFC-modeller med syfte att få stöd med att visuellt och entydigt kunna identifiera system och objekt i modeller.

aoaToTable[](./data/fargkodning-ifc-modeller.json)
