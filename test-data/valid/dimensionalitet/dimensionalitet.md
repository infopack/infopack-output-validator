# Dimensionaliet

Följande värdelista är en del av metoden Informationsbeteckning- och beskrivning som ger svenska anvisningar för tillämpning av **prEN 17412-1 Building Information Modelling – Level of Information Need – Concepts and principles**. Det visar hur man i en leverans­specifikation ger informationsleveransen en beteckning som övergripande beskriver inne­hållet, och sedan i detalj beskriver innehållet.

Geometrisk information som ingår i en leverans kan specificeras med avseende på **dimensionalitet** (obligatorisk), **utseende** (om efterfrågad), **detaljeringsgrad** (om efterfrågad), **lokalisering** (obligatorisk) och **parametriskt beteende** (om efterfrågat).

aoaToTable[](./data/dimensionalitet.json)
