# Status enligt SB11

Statusfältet anger vilken status byggdelar på CAD-lagret har i produktionssituationen. Koderna är desamma som föreskrivs i lagerstandarden SS-ISO 13567 med tre tillägg markerade med * i tabell nedan.

Om fältet inte används anges understreck ( _ ), vilket är fallet vid nybyggnadsprojekt.

aoaToTable[](./data/status-enligt-sb11.json)
