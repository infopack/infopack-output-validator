# Innehåll i dokument

Tabellen nedan redovisar koder för Innehåll i modeller och ritningar. Koderna är baserade på SS 32271:2016. Samt överensstämmer med BSAB-systemets byggdelstabell för de grupper som är markerade med ett kryss i kolumnen längst till höger.

aoaToTable[](./data/innehall-i-dokument.json)
