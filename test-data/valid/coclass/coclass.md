# CoClass

CoClass innehåller tabeller för byggnadsverk, utrymmen, funktionella system, tekniska system, komponenter, produktionsresultat och egenskaper. Du hittar tabellerna på [CoClass webbplats](https://coclass.byggtjanst.se).
